var express = require('express');
var app = express();
var fs = require("fs");

app.get('/', function (req, res) {
    // First read existing users.
   res.end( JSON.stringify({status:false, response:'none',message:"you can't access with out zipcode"}));

 })

app.get('/:id', function (req, res) {
   // First read existing users.
   fs.readFile( __dirname + "/dbfiles/" + "latest_dbfile.json", 'utf8', async function (err, data) {
      var citys = JSON.parse( data );
      var city = citys[req.params.id] 
      res.end( JSON.stringify(city));
   });
})

var server = app.listen(3000, function () {
   var host = server.address().address
   var port = server.address().port
   console.log("Example app listening at http://%s:%s", host, port)
})