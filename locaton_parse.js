const csv = require('csv-parser');
const fs = require('fs');

var all_location = []
fs.createReadStream('dbfiles/Locality_village_pincode_final_mar-2017.csv')
  .pipe(csv())
  .on('data', (row) => {
      localinfo = {
            'Locality': row['Village/Locality name'],
            'Area': row['Officename ( BO/SO/HO)'],
            'Pincode': row['Pincode'],
            'Sub-distname': row['Sub-distname'],
            'Districtname': row['Districtname'],
            'StateName': row['StateName'],
        }
    //   console.log(row)
    all_location.push(localinfo)
  })
  .on('end', () => {
    console.log('CSV file successfully processed');
    // console.log(all_location)
    change_as_keyed(all_location);
  });

var groupBy = async (date, key) => {
    return date.reduce(function(singleElement, x) {
        (singleElement[x[key]] = singleElement[x[key]] || []).push(x);
        return singleElement;
    }, {});
};

var change_as_keyed = async (data) =>{
    var gropued_data = await groupBy(data,'Pincode')
    await change_as_json(gropued_data);
}

var change_as_json = async (data) => {
    var json_data = JSON.stringify(data);
    fs.writeFile('dbfiles/latest_dbfile.json', json_data, 'utf8', async function(){
        console.log('Json file ready to use');
    });
}