# TN Locality pincode based

its a simple app that will get the pincode based local address and give it in a webservice,   
* Don't use it for production
* Its just get you idea to deal with JSON file
* All this data's are pulled from [data.gov.in](https://data.gov.in)
* Get me your ideas to improve this

## Installation
Its a simple express server can be started using following simple comments 

```bash
cd {root path}
npm i
node index.js
```

## Usage
Once the above app started running, get to your browser **http://localhost:3000/pincode** will get you the list of areas 
present in thr

**here the example:**
``` http://localhost:3000/110003```

## Dev Usage
Under the dbfiles folder you can see the source of this data that are populating through the service 

data's that are present in the latest_dbfile.json is what given in the web service

to get the latest zip service, please get the valid CSV file from  [data.gov.in](https://data.gov.in) keep that CSV under the dbfiles  
and change the file name in **locaton_parse.js** and run this file to get the latest dbfile updated


## Contributing
Pull requests are welcome. For major changes, please open an issue first to discuss what you would like to change.

Please make sure to update tests as appropriate.

## License
[MIT](https://choosealicense.com/licenses/mit/)